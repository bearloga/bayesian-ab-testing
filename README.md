# Bayesian A/B testing

```math
p(\theta | y) \propto p(y | \theta) p(\theta)
```
